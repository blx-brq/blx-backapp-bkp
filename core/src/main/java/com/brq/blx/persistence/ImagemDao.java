package com.brq.blx.persistence;

import javax.enterprise.context.Dependent;
import com.brq.blx.entity.Imagem;
import com.brq.blx.infraestrutura.AbstractRepository;

@Dependent
public class ImagemDao extends AbstractRepository<Imagem> {

}