package com.brq.blx.persistence;

import javax.enterprise.context.Dependent;

import com.brq.blx.entity.Contato;
import com.brq.blx.infraestrutura.AbstractRepository;

@Dependent
public class ContatoDao extends AbstractRepository<Contato> {
	
}