package com.brq.blx.persistence;

import javax.enterprise.context.Dependent;

import com.brq.blx.entity.Endereco;
import com.brq.blx.infraestrutura.AbstractRepository;

@Dependent
public class EnderecoDao extends AbstractRepository<Endereco> {
	
}