package com.brq.blx.persistence;

import javax.enterprise.context.Dependent;

import com.brq.blx.entity.Alteracao;
import com.brq.blx.infraestrutura.AbstractRepository;

@Dependent
public class AlteracaoDao extends AbstractRepository<Alteracao> {

}