
package com.brq.blx.persistence;

import javax.enterprise.context.Dependent;

import com.brq.blx.entity.Categoria;
import com.brq.blx.infraestrutura.AbstractRepository;

@Dependent
public class CategoriaDao extends AbstractRepository<Categoria> {

}