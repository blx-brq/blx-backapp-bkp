package com.brq.blx.persistence;

import javax.enterprise.context.Dependent;
import com.brq.blx.entity.TipoUsuario;
import com.brq.blx.infraestrutura.AbstractRepository;

@Dependent
public class TipoUsuarioDao extends AbstractRepository<TipoUsuario> {
	
}