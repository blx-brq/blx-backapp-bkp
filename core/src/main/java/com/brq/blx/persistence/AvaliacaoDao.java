package com.brq.blx.persistence;

import javax.enterprise.context.Dependent;

import com.brq.blx.entity.Avaliacao;
import com.brq.blx.infraestrutura.AbstractRepository;

@Dependent
public class AvaliacaoDao extends AbstractRepository<Avaliacao> {

}