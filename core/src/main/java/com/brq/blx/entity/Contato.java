package com.brq.blx.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BLX_CONTATO")
@NamedQuery(name = "Contato.findAll", query = "SELECT b FROM Contato b")
public class Contato implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "BLX_CONTATO_CODCONTATO_GENERATOR", sequenceName = "SEQ_ID_CONTATO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BLX_CONTATO_CODCONTATO_GENERATOR")
	@Column(name = "COD_CONTATO")
	private long codContato;

	@Column(name = "VL_TELEFONEFIXO")
	private String vlTelefoneFixo;

	@Column(name = "VL_TELEFONEMOVEL")
	private String vlTelefoneMovel;

	/* RELAÇÕES JOINCOLUMN */
	
	@ManyToOne(cascade=CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinColumn(name = "USUARIO_COD_USUARIO") 
	private Usuario blxUsuario;

	public Contato() {}

	public Contato(long codContato, String vlTelefonefixo, String vlTelefonemovel) {
		super();
		this.codContato = codContato;
		this.vlTelefoneFixo = vlTelefonefixo;
		this.vlTelefoneMovel = vlTelefonemovel;
	}
	
	public Contato(long codContato, String vlTelefoneFixo, String vlTelefoneMovel, Usuario blxUsuario) {
		super();
		this.codContato = codContato;
		this.vlTelefoneFixo = vlTelefoneFixo;
		this.vlTelefoneMovel = vlTelefoneMovel;
		this.blxUsuario = blxUsuario;
	}

	public Contato(long codContato, String vlTelefoneFixo, String vlTelefoneMovel, List<Anuncio> blxAnuncios,
			Usuario blxUsuario, List<Endereco> blxEnderecos) {
		super();
		this.codContato = codContato;
		this.vlTelefoneFixo = vlTelefoneFixo;
		this.vlTelefoneMovel = vlTelefoneMovel;
		this.blxUsuario = blxUsuario;
		
	}

	public long getCodContato() {
		return this.codContato;
	}

	public void setCodContato(long codContato) {
		this.codContato = codContato;
	}

	public String getVlTelefoneFixo() {
		return this.vlTelefoneFixo;
	}

	public void setVlTelefoneFixo(String vlTelefonefixo) {
		this.vlTelefoneFixo = vlTelefonefixo;
	}

	public String getVlTelefoneMovel() {
		return this.vlTelefoneMovel;
	}

	public void setVlTelefoneMovel(String vlTelefonemovel) {
		this.vlTelefoneMovel = vlTelefonemovel;
	}

	@Override
	public String toString() {
		return "Contato [codContato=" + codContato + ", vlTelefonefixo=" + vlTelefoneFixo + ", vlTelefonemovel="
				+ vlTelefoneMovel + ", blxUsuario=" + blxUsuario + "]";
	}
}