package com.brq.blx.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(name = "BLX_ENDERECO")
@NamedQuery(name = "Endereco.findAll", query = "SELECT b FROM Endereco b")
public class Endereco implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "BLX_ENDERECO_CODENDERECO_GENERATOR", sequenceName = "SEQ_ID_ENDERECO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BLX_ENDERECO_CODENDERECO_GENERATOR")
	@Column(name = "COD_ENDERECO")
	private long codEndereco;

	@Column(name = "VL_CEP")
	private String vlCep;

	@Column(name = "VL_CIDADE")
	private String vlCidade;

	@Column(name = "VL_RUA")
	private String vlRua;

	@Column(name = "VL_UF")
	private String vlUf;
	
	/* RELAÇÕES JOINCOLUMN */

	@ManyToOne(cascade=CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinColumn(name = "CONTATO_COD_CONTATO") private Contato blxContato;

	public Endereco() {}

	public Endereco(long codEndereco, String vlCep, String vlCidade, String vlRua, String vlUf, Contato blxContato) {
		super();
		this.codEndereco = codEndereco;
		this.vlCep = vlCep;
		this.vlCidade = vlCidade;
		this.vlRua = vlRua;
		this.vlUf = vlUf;
		this.blxContato = blxContato;
	}

	public long getCodEndereco() {
		return this.codEndereco;
	}

	public void setCodEndereco(long codEndereco) {
		this.codEndereco = codEndereco;
	}

	public String getVlCep() {
		return this.vlCep;
	}

	public void setVlCep(String vlCep) {
		this.vlCep = vlCep;
	}

	public String getVlCidade() {
		return this.vlCidade;
	}

	public void setVlCidade(String vlCidade) {
		this.vlCidade = vlCidade;
	}

	public String getVlRua() {
		return this.vlRua;
	}

	public void setVlRua(String vlRua) {
		this.vlRua = vlRua;
	}

	public String getVlUf() {
		return this.vlUf;
	}

	public void setVlUf(String vlUf) {
		this.vlUf = vlUf;
	}

	@Override
	public String toString() {
		return "Endereco [codEndereco=" + codEndereco + ", vlCep=" + vlCep + ", vlCidade=" + vlCidade + ", vlRua="
				+ vlRua + ", vlUf=" + vlUf + ", blxContato=" + blxContato + "]";
	}
}